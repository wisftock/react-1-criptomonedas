import React, { Fragment, useState } from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
  color: #fff;
  margin-right: 5px;
  display: block;
  font-weight: bold;
  margin-bottom: 0.5rem;
`;
const Select = styled.select`
  width: 100%;
  display: block;
  border: none;
  padding: 10px;
  border-radius: 5px;
  margin-bottom: 10px;
`;

const useMoneda = (label, stateInitial, opciones) => {
  const [state, setState] = useState(stateInitial);
  const handleOnChange = (e) => {
    setState(e.target.value);
  };
  const SelectMoneda = () => {
    return (
      <Fragment>
        <Label>{label}</Label>
        <Select onChange={handleOnChange} value={state}>
          <option value=''>Seleccionar</option>
          {opciones.map((opcion) => {
            return (
              <option key={opcion.codigo} value={opcion.codigo}>
                {opcion.nombre}
              </option>
            );
          })}
        </Select>
      </Fragment>
    );
  };
  return [state, SelectMoneda, setState];
};
export default useMoneda;
