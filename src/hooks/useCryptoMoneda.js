import React, { Fragment, useState } from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
  color: #fff;
  margin-right: 5px;
  display: block;
  font-weight: bold;
  margin-bottom: 0.5rem;
`;
const Select = styled.select`
  width: 100%;
  display: block;
  border: none;
  padding: 10px;
  border-radius: 5px;
`;
const useCryptoMoneda = (label, stateInitial, opciones) => {
  const [state, setState] = useState(stateInitial);
  const handleOnChange = (e) => {
    setState(e.target.value);
  };

  const SelectCrypto = () => {
    return (
      <Fragment>
        <Label>{label}</Label>
        <Select onChange={handleOnChange} value={state}>
          <option value=''>Seleccionar</option>
          {opciones.map((opcion) => {
            return (
              <option key={opcion.CoinInfo.Id} value={opcion.CoinInfo.Name}>
                {opcion.CoinInfo.FullName}
              </option>
            );
          })}
        </Select>
      </Fragment>
    );
  };
  return [state, SelectCrypto, setState];
};

export default useCryptoMoneda;
