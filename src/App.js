import { useEffect, useState } from 'react';
import criptomonedas from './criptos.png';
import Formulario from './components/Formulario';
import Cotizacion from './components/Cotizacion';
import axios from 'axios';
import styled from '@emotion/styled';
import Spinner from './components/Spinner';
const Contenedor = styled.div`
  max-width: 80%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  @media (max-width: 992px) {
    display: flex;
    flex-wrap: wrap;
  }
`;

const Imagen = styled.img`
  max-width: 100%;
`;
const Heading = styled.h1`
  color: #fff;
  font-weight: 700;
  margin-bottom: 30px;
  ::after {
    content: '';
    width: 100px;
    height: 6px;
    background-color: #66a2fe;
    display: block;
  }
`;
function App() {
  const [saveMoneda, setSaveMoneda] = useState('');
  const [saveCrypto, setSaveCrypto] = useState('');
  const [information, setInformation] = useState({});
  const [load, setLoad] = useState(false);
  useEffect(() => {
    // evitamos la ejecucion la primera vez
    const consultarAPIS = async () => {
      if (saveMoneda === '') return;
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${saveCrypto}&tsyms=${saveMoneda}`;
      const respuesta = await axios.get(url);
      // mostrar spinner
      setLoad(true);
      setTimeout(() => {
        setLoad(false);
        setInformation(respuesta.data.DISPLAY[saveCrypto][saveMoneda]);
      }, 1500);
    };
    consultarAPIS();
  }, [saveMoneda, saveCrypto]);

  const componente = load ? (
    <Spinner />
  ) : (
    <Cotizacion information={information} />
  );
  return (
    <Contenedor>
      <div>
        <Imagen src={criptomonedas} alt='image crypto' />
      </div>
      <div>
        <Heading>Cotizador de Criptomonedas</Heading>
        <Formulario
          setSaveMoneda={setSaveMoneda}
          setSaveCrypto={setSaveCrypto}
        />
        {componente}
      </div>
    </Contenedor>
  );
}

export default App;
