import React, { useEffect, useState } from 'react';
import axios from 'axios';
import useMoneda from '../hooks/useMoneda';
import useCryptoMoneda from '../hooks/useCryptoMoneda';
import styled from '@emotion/styled';
import Error from './Error';

const Boton = styled.input`
  margin-top: 20px;
  font-weight: bold;
  font-size: 20px;
  padding: 10px;
  background-color: #66a2fe;
  border: none;
  width: 100%;
  border-radius: 5px;
  color: #fff;
  transition: background-color 0.3s ease;
  cursor: pointer;
  &:hover {
    background-color: #326ac0;
  }
`;

const Formulario = ({ setSaveMoneda, setSaveCrypto }) => {
  const [listcryto, setListcryto] = useState([]);
  const [error, setError] = useState(false);
  const MONEDAS = [
    { codigo: 'USD', nombre: 'Dolar de Estados Unidos' },
    { codigo: 'MXN', nombre: 'Peso Mexicano' },
    { codigo: 'EUR', nombre: 'Euro' },
    { codigo: 'GBP', nombre: 'Libra Esterlina' },
  ];
  const [moneda, SelectMoneda] = useMoneda('Elije moneda', '', MONEDAS);
  const [crypto, SelectCrypto] = useCryptoMoneda(
    'Selecciona cryptomoneda',
    '',
    listcryto
  );
  // Ejecutar llamado a la API
  useEffect(() => {
    const consultarAPI = async () => {
      const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;
      const resultado = await axios.get(url);
      setListcryto(resultado.data.Data);
    };
    consultarAPI();
  }, []);
  const handleSubmit = (e) => {
    e.preventDefault();
    if (moneda === '' || crypto === '') {
      setError(true);
      return;
    }
    setError(false);
    setSaveMoneda(moneda);
    setSaveCrypto(crypto);
  };
  return (
    <div>
      {error && <Error mensaje='Todos los campos son obligatorios' />}
      <form onSubmit={handleSubmit}>
        <SelectMoneda />
        <SelectCrypto />
        <Boton type='submit' value='Calcular' />
      </form>
    </div>
  );
};

export default Formulario;
