import React from 'react';
import styled from '@emotion/styled';

const ErrorMensaje = styled.p`
  background-color: #ef0b22;
  padding: 1px;
  color: #fff;
  font-size: 1.2rem;
  text-align: center;
  border-radius: 5px;
  margin-bottom: 5px;
`;
const Error = ({ mensaje }) => {
  return <ErrorMensaje>{mensaje}</ErrorMensaje>;
};

export default Error;
