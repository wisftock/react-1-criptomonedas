import React from 'react';
import styled from '@emotion/styled';

const ResultadoDIV = styled.div`
  margin-top: 5px;
  color: #ffffff;
`;
const Info = styled.p`
  font-size: 18px;
`;
const Precios = styled.p`
  font-size: 20px;
`;
const Cotizacion = ({ information }) => {
  // console.log(information);
  if (Object.keys(information).length === 0) return null;

  const { PRICE, HIGHDAY, LOWDAY, CHANGEPCT24HOUR, LASTUPDATE } = information;
  return (
    <ResultadoDIV>
      <Precios>
        El precio es: <span>{PRICE}</span>
      </Precios>
      <Info>
        El precio más alto del día: <span>{HIGHDAY}</span>
      </Info>
      <Info>
        El precio más bajo del día: <span>{LOWDAY}</span>
      </Info>
      <Info>
        Variación últimas 24h: <span>{CHANGEPCT24HOUR}</span>
      </Info>
      <Info>
        Última actualización: <span>{LASTUPDATE}</span>
      </Info>
    </ResultadoDIV>
  );
};

export default Cotizacion;
